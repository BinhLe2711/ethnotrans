/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StatusBar } from 'react-native';
import type { Node } from 'react';
import Main from './src/Main';
import { store } from './src/redux/store';
import { Provider } from 'react-redux';

const App: () => Node = () => {
    return (
        <Provider store={store}>
            <StatusBar hidden />
            <Main />
        </Provider>
    );
};

export default App;
