import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import AuthenticationScreen from './screens/AuthenticationScreen';
import TranslateScreen from './screens/TranslateScreen';
import StorageScreen from './screens/StorageScreen';
import { Image } from 'react-native';

import { AsyncStorage } from 'react-native';

import { setLogin } from './redux/slice/auth.slice';
import { useSelector, useDispatch } from 'react-redux';

const Tab = createBottomTabNavigator();

function MyTabs() {
    return (
        <Tab.Navigator
            screenOptions={{
                tabBarStyle: { backgroundColor: '#EAE7D6' },
            }}>
            <Tab.Screen
                name="Dịch thuật"
                component={TranslateScreen}
                options={{
                    tabBarActiveTintColor: '#000000',
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => {
                        if (color === '#000000') {
                            return (
                                <Image
                                    source={require('./assets/images/bottom/comment.png')}
                                    style={{ width: 26, height: 26 }}
                                />
                            );
                        } else {
                            return (
                                <Image
                                    source={require('./assets/images/bottom/comment_.png')}
                                    style={{ width: 26, height: 26 }}
                                />
                            );
                        }
                    },
                }}
            />
            <Tab.Screen
                name="Lưu trữ"
                component={StorageScreen}
                options={{
                    tabBarActiveTintColor: '#000000',
                    headerShown: false,
                    tabBarIcon: ({ color, size }) => {
                        if (color === '#000000') {
                            return (
                                <Image
                                    source={require('./assets/images/bottom/storage.png')}
                                    style={{ width: 26, height: 26 }}
                                />
                            );
                        } else {
                            return (
                                <Image
                                    source={require('./assets/images/bottom/storage_.png')}
                                    style={{ width: 26, height: 26 }}
                                />
                            );
                        }
                    },
                }}
            />
        </Tab.Navigator>
    );
}

const Stack = createNativeStackNavigator();

function Main() {
    const dispatch = useDispatch();
    const isLogin = useSelector(state => state.auth.isLogin);
    useEffect(() => {
        async function x() {
            let a = await AsyncStorage.getItem('isLogin');
            if (a != null && a != undefined) {
                dispatch(setLogin(1));
            }
        }
        x();
    }, [dispatch]);
    if (!isLogin) {
        return <AuthenticationScreen />;
    }
    return (
        <NavigationContainer>
            <MyTabs />
        </NavigationContainer>
    );
}

export default Main;
