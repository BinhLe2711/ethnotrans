/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    ImageBackground,
    StyleSheet,
    Dimensions,
    Text,
    SafeAreaView,
    AsyncStorage,
    ScrollView,
    FlatList,
} from 'react-native';

const { width, height } = Dimensions.get('screen');
import { useSelector } from 'react-redux';

export default function StorageScreen() {
    const [greeting_words, set_greeting_words] = useState([]);
    const [relationship_words, set_relationship_words] = useState([]);

    const fakeRedux = useSelector(state => state.auth.fakeRedux);

    useEffect(() => {
        async function x() {
            let greeting = JSON.parse(await AsyncStorage.getItem('greeting'));
            if (greeting == null || greeting === undefined) {
                greeting = [];
            }
            set_greeting_words(greeting);

            let relationship = JSON.parse(
                await AsyncStorage.getItem('relationship'),
            );
            if (relationship == null || relationship === undefined) {
                relationship = [];
            }
            set_relationship_words(relationship);
        }
        x();
    }, [fakeRedux]);

    const Item = ({ item }) => {
        return (
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: 'BeVietnamPro-Regular',
                    marginTop: 10,
                }}>
                {item.text} ({item.language})
            </Text>
        );
    };

    const Item2 = ({ item }) => {
        return (
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: 'BeVietnamPro-Regular',
                    marginTop: 10,
                }}>
                {item.text} ({item.language})
            </Text>
        );
    };

    return (
        <View>
            <ImageBackground
                source={require('../assets/images/storage/banner.png')}
                style={styles.bannerContainer}>
                <SafeAreaView style={{ alignItems: 'center', marginTop: 60 }}>
                    <Text
                        style={{
                            fontSize: 30,
                            fontFamily: 'BeVietnamPro-Black',
                            color: 'white',
                        }}>
                        Ethnotrans
                    </Text>
                </SafeAreaView>
            </ImageBackground>
            <View
                style={{
                    width,
                    marginTop: 20,
                    flexDirection: 'row',
                }}>
                <View
                    style={{
                        backgroundColor: '#B0D4B8',
                        borderRadius: 30,
                        paddingHorizontal: 30,
                        paddingVertical: 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 10,
                        flex: 1,
                    }}>
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: 'BeVietnamPro-Bold',
                            marginBottom: 20,
                        }}>
                        Chào hỏi
                    </Text>
                    <FlatList
                        data={greeting_words}
                        renderItem={Item}
                        keyExtractor={(item, i) => i}
                    />
                </View>
                <View
                    style={{
                        backgroundColor: '#B0D4B8',
                        borderRadius: 30,
                        paddingHorizontal: 30,
                        paddingVertical: 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 10,
                        flex: 1,
                    }}>
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: 'BeVietnamPro-Bold',
                            marginBottom: 20,
                        }}>
                        Mối quan hệ
                    </Text>
                    <FlatList
                        data={relationship_words}
                        renderItem={Item2}
                        keyExtractor={(item, i) => i}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    bannerContainer: {
        width: width,
        height: width * 0.69565217391,
    },
});
