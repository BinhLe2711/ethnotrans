import React, { useState } from 'react';
import {
    SafeAreaView,
    View,
    Text,
    ImageBackground,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';

import LoginModal from '../components/auth/LoginModal';
import RegisterModal from '../components/auth/RegisterModal';

const { height, width } = Dimensions.get('screen');

export default function AuthenticationScreen() {
    const [showLoginModal, setShowLoginModal] = useState(0);
    const [showRegisterModal, setShowRegisterModal] = useState(0);
    return (
        <ImageBackground
            source={require('../assets/images/home/background.png')}
            style={styles.container}>
            <View style={[styles.container, { justifyContent: 'flex-end' }]}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: height - height / 1.5 - 30,
                    }}>
                    <Text style={styles.title}>Ethnotrans</Text>
                    <Text style={styles.description}>
                        Cùng nhau tìm hiểu 54 dân tộc Việt Nam
                    </Text>
                </View>
                <View style={styles.card}>
                    <TouchableOpacity
                        style={styles.buttonPrimary}
                        activeOpacity={0.7}
                        onPress={() => setShowRegisterModal(true)}>
                        <Text style={styles.txtButtonPrimary}>Đăng ký</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.buttonSecondary}
                        activeOpacity={0.7}
                        onPress={() => setShowLoginModal(true)}>
                        <Text style={styles.txtButtonPrimary}>Đăng nhập</Text>
                    </TouchableOpacity>
                    <ImageBackground
                        source={require('../assets/images/home/map.png')}
                        style={styles.footer}>
                        <View style={{ marginBottom: 150 }}>
                            <TouchableOpacity
                                activeOpacity={0.7}
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    borderBottomWidth: 1,
                                    paddingBottom: 4,
                                }}>
                                <Image
                                    source={require('../assets/images/home/facebook.png')}
                                    style={{
                                        width: 25,
                                        height: 25,
                                        marginRight: 10,
                                    }}
                                />
                                <Text style={styles.loginText}>
                                    Tiếp tục với Facebook
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
            </View>
            <LoginModal
                visible={showLoginModal}
                hiddenModal={() => {
                    setShowLoginModal(false);
                }}
            />
            <RegisterModal
                visible={showRegisterModal}
                hiddenModal={() => {
                    setShowRegisterModal(false);
                }}
            />
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    container: {
        minHeight: height,
    },
    card: {
        width: width,
        height: height / 1.5,
        backgroundColor: 'white',
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        paddingHorizontal: '5%',
        paddingVertical: '10%',
        alignItems: 'center',
    },
    title: {
        color: 'white',
        fontSize: 35,
        fontFamily: 'BeVietnamPro-Black',
    },
    description: {
        color: 'white',
        fontSize: 18,
        marginTop: 10,
        fontFamily: 'BeVietnamPro-Bold',
    },
    buttonPrimary: {
        backgroundColor: '#A4C3A2',
        borderRadius: 1000,
        paddingHorizontal: 45,
        paddingVertical: 15,
        minWidth: 220,
        alignItems: 'center',
        shadowColor: '#5D7B6F',
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 1,

        elevation: 4,
    },
    txtButtonPrimary: {
        fontSize: 25,
        fontFamily: 'BeVietnamPro-SemiBold',
    },
    buttonSecondary: {
        marginTop: '7%',
        backgroundColor: '#fff',
        borderRadius: 1000,
        paddingHorizontal: 45,
        paddingVertical: 15,
        minWidth: 220,
        shadowColor: '#BDBDBD',
        borderWidth: 1,
        borderColor: '#828282',
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 1,
        shadowRadius: 1,

        elevation: 4,
    },
    footer: {
        height: '85%',
        justifyContent: 'flex-end',
    },
    loginText: {
        fontSize: 18,
        fontFamily: 'BeVietnamPro-Thin',
    },
});
