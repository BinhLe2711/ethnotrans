/* eslint-disable react-native/no-inline-styles */
import React, { useState, createRef, useCallback } from 'react';
import {
    View,
    Text,
    SafeAreaView,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Image,
    AsyncStorage,
} from 'react-native';
const { width, height } = Dimensions.get('screen');
import Toast from 'react-native-tiny-toast';

import FloattingButton from '../components/FloattingButton';

import SelectModal from '../components/SelectModal';
import DelayInput from 'react-native-debounce-input';

import { fakeR } from '../redux/slice/auth.slice';
import { useDispatch } from 'react-redux';

const greeting_words = [
    'Kkuh',
    'Kkuh kơ ih amai ah! kâo wĭt',
    'ផំរាបសួរ',
    'Chom rép sua',
    'KơKuh',
    'KơKuh',
    'E đâng yơ?',
    'Xo tuộng',
    'Xo tuộng cần đai',
    "Xo t'uộng nâư",
    '你 好',
    'Nyob zoo',
    'Chàu vòch',
    'Niăm să',
    'Aỳaq',
    "K'kôh",
    'Mbah nai',
    'Buênh tê',
    'ສະບາຍດີ',
    'Xin chào',
];
const relationship_words = [
    'Amĭ',
    'Mẹ',
    'Pỉ noọng',
    'Anh em',
    'Óq tree tree',
    'Em gái',
    'Bạn bè',
    'Ka lơ',
    'Ạ pa',
    'Bố',
    'Ạ me',
];

export default function TranslateScreen() {
    const [modal1, setModal1] = useState(false);
    const [name1, setName1] = useState('Kinh');
    const [value1, setValue1] = useState(1);

    const [modal2, setModal2] = useState(false);
    const [name2, setName2] = useState('Kinh');
    const [value2, setValue2] = useState(1);

    const [result, setResult] = useState('');

    const [translateText, setTranslateText] = useState('');
    const inputRef = createRef();

    const dispatch = useDispatch();

    const saveGreeting = async (result, lang) => {
        return new Promise(async (resolve, reject) => {
            let greeting = JSON.parse(await AsyncStorage.getItem('greeting'));
            if (greeting == null || greeting === undefined) {
                greeting = [];
            }

            let find_greeting_word = greeting_words.find(
                tmp => tmp.toLowerCase().trim() === result.toLowerCase().trim(),
            );

            let find_on_history = greeting.find(
                tmp =>
                    tmp.text.toLowerCase().trim() ===
                    result.toLowerCase().trim(),
            );

            if (
                find_greeting_word !== null &&
                find_greeting_word !== undefined &&
                (find_on_history === null || find_on_history === undefined)
            ) {
                greeting.push({
                    language: lang,
                    text: result,
                });
                await AsyncStorage.setItem(
                    'greeting',
                    JSON.stringify(greeting),
                );
            }
            resolve();
        });
    };
    const saveRelationship = async (result, lang) => {
        return new Promise(async (resolve, reject) => {
            let relationship = JSON.parse(
                await AsyncStorage.getItem('relationship'),
            );
            if (relationship == null || relationship === undefined) {
                relationship = [];
            }

            let find_relationship_word = relationship_words.find(
                tmp => tmp.toLowerCase().trim() === result.toLowerCase().trim(),
            );

            let find_on_history = relationship.find(
                tmp =>
                    tmp.text.toLowerCase().trim() ===
                    result.toLowerCase().trim(),
            );

            if (
                find_relationship_word !== null &&
                find_relationship_word !== undefined &&
                (find_on_history === null || find_on_history === undefined)
            ) {
                relationship.push({
                    language: lang,
                    text: result,
                });
                await AsyncStorage.setItem(
                    'relationship',
                    JSON.stringify(relationship),
                );
            }
            resolve();
        });
    };

    const handleTranslate = async text => {
        setTranslateText(text);
        dispatch(fakeR());
        setResult('Đang dịch...');
        if (value1 === value2) {
            setResult(text);
            return;
        }
        const data = new FormData();
        data.append('from', value1);
        data.append('to', value2);
        data.append('value', text);
        fetch('https://eth.kb2ateam.org/api/translate', {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(async responseData => {
                console.log(responseData);
                if (responseData.data.result === null) {
                    setResult('Không có từ này');
                } else {
                    await saveGreeting(
                        responseData.data.result,
                        responseData.data.to,
                    );
                    await saveGreeting(text, responseData.data.from);
                    await saveRelationship(
                        responseData.data.result,
                        responseData.data.to,
                    );
                    await saveRelationship(text, responseData.data.from);
                    setResult(responseData.data.result);
                }
            });
    };
    return (
        <SafeAreaView style={styles.container}>
            <SelectModal
                visible={modal1}
                hiddenModal={() => setModal1(0)}
                setData={({ name, value }) => {
                    setName1(name);
                    setValue1(value);
                    setResult('');
                    setTranslateText('');
                }}
                initialValue={value1}
                initialName={name1}
            />
            <SelectModal
                visible={modal2}
                hiddenModal={() => setModal2(0)}
                setData={({ name, value }) => {
                    setName2(name);
                    setValue2(value);
                    setResult('');
                    setTranslateText('');
                }}
                initialValue={value2}
                initialName={name2}
            />
            <View style={styles.topBar}>
                <View style={{ position: 'relative' }}>
                    <Text
                        style={{
                            fontSize: 26,
                            color: '#5D7B6F',
                            fontFamily: 'BeVietnamPro-Black',
                            position: 'absolute',
                            transform: [{ translateY: 3 }],
                        }}>
                        Ethnotrans
                    </Text>
                    <Text
                        style={{
                            fontSize: 26,
                            color: '#A4C3A2',
                            fontFamily: 'BeVietnamPro-Black',
                        }}>
                        Ethnotrans
                    </Text>
                </View>
            </View>
            <View style={styles.translate}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingVertical: 15,
                        borderBottomWidth: 1,
                        borderBottomColor: 'white',
                    }}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => setModal1(1)}>
                        <Text style={styles.translateText}>{name1}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() =>
                            Toast.show('Tính năng này đang được phát triển')
                        }>
                        <Image
                            source={require('../assets/images/translate/swap.png')}
                            style={{
                                width: 25,
                                height: 25,
                                marginHorizontal: 15,
                            }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => setModal2(1)}>
                        <Text style={styles.translateText}>{name2}</Text>
                    </TouchableOpacity>
                </View>

                <DelayInput
                    value={translateText}
                    inputRef={inputRef}
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={handleTranslate}
                    delayTimeout={500}
                    style={{
                        height: height / 3 - (25 + 15 + 15) * 2,
                        paddingVertical: 20,
                        paddingHorizontal: 10,
                        color: 'white',
                    }}
                />

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: 15,
                        paddingHorizontal: 60,
                        borderTopWidth: 1,
                        borderTopColor: 'white',
                    }}>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() =>
                            Toast.show('Tính năng này đang được phát triển')
                        }>
                        <Image
                            source={require('../assets/images/translate/vol.png')}
                            style={{
                                width: 25,
                                height: 25,
                            }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() =>
                            Toast.show('Tính năng này đang được phát triển')
                        }>
                        <Image
                            source={require('../assets/images/translate/mic.png')}
                            style={{
                                width: 25,
                                height: 25,
                            }}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() =>
                            Toast.show('Tính năng này đang được phát triển')
                        }>
                        <Image
                            source={require('../assets/images/translate/cam.png')}
                            style={{
                                width: 25,
                                height: 25,
                            }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.wrapperResult}>
                <Text
                    style={{
                        fontSize: 16,
                        fontFamily: 'BeVietnamPro-Regular',
                    }}>
                    {result}
                </Text>
            </View>
            <FloattingButton />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        minHeight: height,
    },
    topBar: {
        marginTop: 10,
        alignItems: 'center',
    },
    translate: {
        backgroundColor: '#5D7B6F',
        width,
        height: height / 3,
        marginTop: 15,
    },
    translateText: {
        color: 'white',
        fontFamily: 'BeVietnamPro-SemiBold',
        fontSize: 18,
    },
    wrapperResult: {
        backgroundColor: '#A4C3A2',
        width,
        height: height / 4,
        marginTop: 15,
        paddingVertical: 20,
        paddingHorizontal: 20,
    },
});
