import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLogin: 0,
    fakeRedux: 0,
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setLogin: (state, action) => {
            state.isLogin = action.payload;
        },
        fakeR: state => {
            state.fakeRedux++;
        },
    },
});

// Action creators are generated for each case reducer function
export const { setLogin, fakeR } = authSlice.actions;

export default authSlice.reducer;
