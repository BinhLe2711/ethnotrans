import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TextInput,
    TouchableOpacity,
} from 'react-native';

const { width, height } = Dimensions.get('screen');
import Toast from 'react-native-tiny-toast';

export default function RegisterModal({ visible, hiddenModal }) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');

    const handleRegister = async () => {
        const data = new FormData();
        data.append('name', name);
        data.append('password', password);
        data.append('email', email);
        fetch('https://eth.kb2ateam.org/api/auth/register', {
            method: 'POST',
            body: data,
        })
            .then(response => response.json())
            .then(json => {
                if (json.message === 'User successfully registered') {
                    Toast.show('Đăng ký thành công vui lòng đăng nhập');
                } else {
                    Toast.show(json[Object.keys(json)[0]][0]);
                }
            });
    };

    if (!visible) {
        return null;
    }
    return (
        <>
            <View style={styles.container}>
                <View>
                    <Text style={styles.textInputtxt}>Tên</Text>
                    <View>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={e => setName(e)}
                            value={name}
                        />
                    </View>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Text style={styles.textInputtxt}>Email</Text>
                    <View>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={e => setEmail(e)}
                            value={email}
                        />
                    </View>
                </View>
                <View style={{ marginTop: 20 }}>
                    <Text style={styles.textInputtxt}>Password</Text>
                    <View>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={e => setPassword(e)}
                            value={password}
                            secureTextEntry={true}
                        />
                    </View>
                </View>
                <TouchableOpacity
                    style={styles.button}
                    activeOpacity={0.7}
                    onPress={handleRegister}>
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: 'BeVietnamPro-Bold',
                        }}>
                        Đăng ký
                    </Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={hiddenModal}
                style={{
                    backgroundColor: 'black',
                    width,
                    height,
                    position: 'absolute',
                    opacity: 0.5,
                    zIndex: 1,
                }}
            />
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: height / 2,
        left: width / 2,
        width: width / 1.5,
        backgroundColor: 'white',
        zIndex: 2,
        transform: [{ translateX: -width / 3 }, { translateY: -height / 5 }],
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10,
    },
    textInput: {
        backgroundColor: '#F5F5F5',
        marginTop: 10,
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontSize: 16,
        borderRadius: 10,
        fontFamily: 'BeVietnamPro-Regular',
    },
    textInputtxt: {
        fontSize: 16,
        fontFamily: 'BeVietnamPro-Regular',
    },
    button: {
        marginTop: 20,
        backgroundColor: '#A4C3A2',
        textAlign: 'center',
        alignItems: 'center',
        paddingVertical: 10,
        borderRadius: 1000,
    },
});
