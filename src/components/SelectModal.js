/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    TextInput,
    TouchableOpacity,
} from 'react-native';

const { width, height } = Dimensions.get('screen');
import Toast from 'react-native-tiny-toast';

import Picker from '@gregfrench/react-native-wheel-picker';
var PickerItem = Picker.Item;
const WheelPicker = ({ onChange, initialValue }) => {
    const [selectedItem, setSelectedItem] = useState(initialValue);
    const [itemList, setItemList] = useState([
        { id: 1, name: 'Kinh' },
        {
            id: 5,
            name: '\u00ca-\u0111\u00ea',
        },
        {
            id: 6,
            name: 'Khmer',
        },
        {
            id: 7,
            name: 'Gia-rai',
        },
        {
            id: 8,
            name: 'Ba na',
        },
        {
            id: 9,
            name: 'T\u00e0y',
        },
        {
            id: 10,
            name: 'M\u01b0\u1eddng',
        },
        {
            id: 11,
            name: 'N\u00f9ng',
        },
        {
            id: 12,
            name: 'Th\u00e1i',
        },
        {
            id: 13,
            name: 'Hoa (H\u00e1n)',
        },
        {
            id: 14,
            name: 'HM\u00f4ng (M\u00e8o)',
        },
        {
            id: 15,
            name: 'Dao',
        },
        {
            id: 16,
            name: 'S\u00e1n Chay (Cao lan - S\u00e1n ch\u1ec9)',
        },
        {
            id: 17,
            name: 'Ch\u0103m (Ch\u00e0m)',
        },
        {
            id: 18,
            name: 'X\u00ea-\u0111\u0103ng',
        },
        {
            id: 19,
            name: 'S\u00e1n D\u00ecu',
        },
        {
            id: 20,
            name: 'Hr\u00ea',
        },
        {
            id: 21,
            name: 'C\u01a1-ho',
        },
        {
            id: 22,
            name: 'Ra Glay',
        },
        {
            id: 23,
            name: 'M\u2019N\u00f4ng',
        },
        {
            id: 24,
            name: 'Bru-V\u00e2n Ki\u1ec1u',
        },
        {
            id: 25,
            name: 'C\u01a1 tu',
        },
        {
            id: 26,
            name: 'Gi\u00e9-Tri\u00eang',
        },
        {
            id: 27,
            name: 'T\u00e0 \u00d4i',
        },
        {
            id: 28,
            name: 'Ch\u01a1 Ro',
        },
        {
            id: 29,
            name: 'H\u00e0 Nh\u00ec',
        },
        {
            id: 30,
            name: 'Xinh Mun',
        },
        {
            id: 31,
            name: 'Chu-ru',
        },
        {
            id: 32,
            name: 'L\u00e0o',
        },
        {
            id: 33,
            name: 'P\u00e0 Th\u1ebbn',
        },
        {
            id: 34,
            name: 'C\u1ed1ng',
        },
        {
            id: 35,
            name: 'Pu P\u00e9o',
        },
        {
            id: 36,
            name: 'Th\u1ed5',
        },
        {
            id: 37,
            name: 'Xti\u00eang',
        },
        {
            id: 38,
            name: 'Kh\u01a1 M\u00fa',
        },
        {
            id: 39,
            name: 'Gi\u00e1y',
        },
        {
            id: 40,
            name: 'M\u1ea1',
        },
        {
            id: 41,
            name: 'M\u1ea1',
        },
        {
            id: 42,
            name: 'Co',
        },
        {
            id: 43,
            name: 'La Ch\u00ed',
        },
        {
            id: 44,
            name: 'Ph\u00f9 L\u00e1',
        },
        {
            id: 45,
            name: 'La H\u1ee7',
        },
        {
            id: 46,
            name: 'Kh\u00e1ng',
        },
        {
            id: 47,
            name: 'L\u1ef1',
        },
        {
            id: 48,
            name: 'L\u00f4 L\u00f4',
        },
        {
            id: 49,
            name: 'Ch\u1ee9t',
        },
        {
            id: 50,
            name: 'M\u1ea3ng',
        },
        {
            id: 51,
            name: 'C\u1edd Lao',
        },
        {
            id: 52,
            name: 'B\u1ed1 Y',
        },
        {
            id: 53,
            name: 'La Ha',
        },
        {
            id: 54,
            name: 'Ng\u00e1i',
        },
        {
            id: 55,
            name: 'Si La',
        },
        {
            id: 56,
            name: 'Br\u00e2u',
        },
        {
            id: 57,
            name: 'R\u01a1 M\u0103m',
        },
        {
            id: 58,
            name: '\u01a0 \u0110u',
        },
    ]);

    return (
        <View>
            <Picker
                style={{ width: 150, height: 180 }}
                lineColor="#000000" //to set top and bottom line color (Without gradients)
                lineGradientColorFrom="#008000" //to set top and bottom starting gradient line color
                lineGradientColorTo="#FF5733" //to set top and bottom ending gradient
                selectedValue={selectedItem}
                itemStyle={{ color: 'black', fontSize: 26 }}
                onValueChange={index => {
                    onChange(itemList.find(data => data.id === index));
                    setSelectedItem(index);
                }}>
                {itemList.map((value, i) => (
                    <PickerItem label={value.name} value={value.id} key={i} />
                ))}
            </Picker>
        </View>
    );
};

export default function SelectModal({
    visible,
    hiddenModal,
    setData,
    initialValue,
    initialName,
}) {
    const [name, setName] = useState(initialName);
    const [value, setValue] = useState(initialValue);
    const handleDone = () => {
        setData({ name, value });
        console.log(initialName);
        hiddenModal();
    };
    const handleChangeValue = data => {
        setName(data.name);
        setValue(data.id);
    };
    if (!visible) {
        return null;
    }
    return (
        <>
            <View style={styles.container}>
                <View style={{ marginTop: 0, alignItems: 'center' }}>
                    <WheelPicker
                        onChange={handleChangeValue}
                        initialValue={initialValue}
                    />
                </View>
                <TouchableOpacity
                    style={styles.button}
                    activeOpacity={0.7}
                    onPress={handleDone}>
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: 'BeVietnamPro-Bold',
                        }}>
                        Xong
                    </Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={hiddenModal}
                style={{
                    backgroundColor: 'black',
                    width,
                    height,
                    position: 'absolute',
                    opacity: 0.5,
                    zIndex: 1,
                }}
            />
        </>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: height / 2,
        left: width / 2,
        width: width / 1.5,
        backgroundColor: 'white',
        zIndex: 2,
        transform: [{ translateX: -width / 3 }, { translateY: -height / 5 }],
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10,
    },
    textInput: {
        backgroundColor: '#F5F5F5',
        marginTop: 10,
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontSize: 16,
        borderRadius: 10,
        fontFamily: 'BeVietnamPro-Regular',
    },
    textInputtxt: {
        fontSize: 16,
        fontFamily: 'BeVietnamPro-Regular',
    },
    button: {
        marginTop: 50,
        backgroundColor: '#A4C3A2',
        textAlign: 'center',
        alignItems: 'center',
        paddingVertical: 10,
        borderRadius: 1000,
    },
});
