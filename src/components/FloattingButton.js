import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import Toast from 'react-native-tiny-toast';

export default function FloattingButton() {
    return (
        <TouchableOpacity
            style={styles.container}
            activeOpacity={0.7}
            onPress={() => Toast.show('Tính năng này đang được phát triển')}>
            <Image source={require('../assets/images/bottom/edit.png')} />
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 1000,
        backgroundColor: '#EAE7D6',
        position: 'absolute',
        bottom: 100,
        right: 10,
        padding: 20,
    },
});
